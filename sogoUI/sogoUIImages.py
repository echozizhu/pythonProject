import requests
import json
import urllib

# def getSogouImag(path):
#
#     imgs = requests.get('https://pic.sogou.com/pic/action/getWapHomeFeed.jsp?key=homeFeedData&category=feed&start=0&len=10')
#     print("imgs" + imgs)
#     jd = json.loads(imgs.text)
#     jd = jd['list']
#     imgs_url = []
#     # for j in jd:
#     #     imgs_url.append(j['bthumbUrl'])
#     # m = 0
#     # for img_url in imgs_url:
#     #         print('***** '+str(m)+'.jpg *****'+'   Downloading...')
#     #         urllib.request.urlretrieve(img_url,path+str(m)+'.jpg')
#     #         m = m + 1
#     # print('Download complete!')complete
#
# getSogouImag('壁纸',10,'sogo/')
def getSogouImag(start,path):
    # 此处uri为 请求uri
    imgs = requests.get('https://pic.sogou.com/pic/action/getWapHomeFeed.jsp?key=homeFeedData&category=feed&start='+str(start)+'&len=100')
    # 以下为数据格式 解析存储数据 遍历并保存图片url到数组中 由urlretrieve去请求url获取图片
    jd = json.loads(imgs.text)
    jd = jd['list']
    imgs_url = []
    for j in jd:
        s = j[0]['picList']
        for t in s:
            imgs_url.append(t['image'])

    m = 0
    for img_url in imgs_url:
        print('***** ' + str(m) + '.jpg *****' + '   Downloading...')
        urllib.request.urlretrieve(img_url,path+str(m)+'.jpg')
        m = m + 1
    print('Download complete!')
    # start 参数 是从多少开始爬 这个可以从请求地址上发现 此处参数 规律为：0 - 10 - 20 递增下去
    # 可以考虑参数修改len参数 来尝试扩大 这是一个分页参数 起始数可不变 获取更多
    # 二次执行 最好更改 文件名生成方式 不然会覆盖旧文件
    # 或者更改存放路径 需要手动创建文件夹 可以新增代码创建文件夹方式
getSogouImag(0,'all/')