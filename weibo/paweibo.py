import requests
import json
import urllib

def getWeiBoImag(start,path):
    # 此处uri为 请求uri
    imgs = requests.get(
        'https://pic.sogou.com/pic/action/getWapHomeFeed.jsp?key=homeFeedData&category=feed&start=' + str(
            start) + '&len=100')
    # 以下为数据格式 解析存储数据 遍历并保存图片url到数组中 由urlretrieve去请求url获取图片
    jd = json.loads(imgs.text)
    jd = jd['list']
    imgs_url = []
    for j in jd:
        s = j[0]['picList']
        for t in s:
            imgs_url.append(t['image'])

    m = 0
    for img_url in imgs_url:
        print('***** ' + str(m) + '.jpg *****' + '   Downloading...')
        urllib.request.urlretrieve(img_url, path + str(m) + '.jpg')
        m = m + 1
    print('Download complete!')

getWeiBoImag(0, 'all/')