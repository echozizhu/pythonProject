# while 10以内循环
# n = 1
# while n < 10:
#     print(n)
#     n = n + 1
# else:
#     print("结束")

# 输出乘法口诀表
# for嵌套循环
# range(起始,结束（不包含）,步长)
# end 添加结束语
# f语法 3.6版本的
# for i in range(1, 10):
#     for j in range(1, i+1):
#         print(f'{i}*{j}={i*j}',  end=' ')
#     # 换行
#     print()
# while 版本的循环嵌套
# i = 1
# while i<=9 :
#     j = 1
#     while j <= i:
#         print(f'{i}*{j}={i*j}',  end=' ')
#         j = j + 1
#     i = i + 1
#     print()
# while for 结合使用版
# i = 1
# while i <= 9:
#     for j in range(1, i+1):
#         print(f'{i}*{j}={i * j}', end=' ')
#     i = i + 1
#     print()

# 循环控制语句
# break 结束循环
# input 输入方法
# while True:
#     s = input("输入：(0)退出")
#     if s == '0':
#         break
#     print('你输入的是', s)

# continue 循环控制
# 跳过后面执行，立刻进入下一循环
# for s in 'liugui':
#     if s == 'g':
#         continue
#     print(s)

"""
猜大小
100以内随机数 用户输入去猜测
猜对结束游戏
int() 方法将内容转成整数
str() 方法将内容转成字符串
random 随机数模块
import 导入关键字 用来导入模块
"""
import random

num = random.randint(1, 100)

# 添加需求 仅允许猜5次
total = 5
count = 0
while True:
    n = int(input('请输入一个1~100的整数:'))
    if n > num:
        print('猜大了')
    elif n < num:
        print('小了')
    else:
        print('猜对了！！！')
        break
    # count = count + 1
    count += 1
    if count >= total:
        print(f'只有{total}次机会!')
        break