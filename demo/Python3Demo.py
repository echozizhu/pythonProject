
##打印关键字
##import keyword
##print(keyword.kwlist)

##if语句
if True:
    print("true")
else:
    print("false")

##python最具特色的就是使用缩进来表示代码块，不需要使用大括号 {} 。
##缩进的空格数是可变的，但是同一个代码块的语句必须包含相同的缩进空格数。
if False:
    print("Answer")
    print("True")
else:
    print("Answer")
    print("False")
##缩进不一致，会导致运行错误 idea工具 也会有提示
##IndentationError: unindent does not match any outer indentation level

##数据类型
## int bool float complex

## 字符串


str = 'Runoob'

print(str)  # 输出字符串
print(str[0:-1])  # 输出第一个到倒数第二个的所有字符
print(str[0])  # 输出字符串第一个字符
print(str[2:5])  # 输出从第三个开始到第五个的字符
print(str[2:])  # 输出从第三个开始后的所有字符
print(str * 2)  # 输出字符串两次
print(str + '你好')  # 连接字符串

print('------------------------------')

print('hello\nrunoob')  # 使用反斜杠(\)+n转义特殊字符
print(r'hello\nrunoob')  # 在字符串前面添加一个 r，表示原始字符串，不会发生转义

import sys
x = 'liugui'
sys.stdout.write(x+"\n")
## 输出liugui 如果使用交互式窗口 会输出 字符串长度 6

## 模块导入 与 导入模块成员
## import 与 from...import


import sys
print('================Python import mode==========================')
print ('命令行参数为:')
for i in sys.argv:
    print (i)
print ('\n python 路径为',sys.path)


from sys import argv,path
print("path:",path)
print("python -h")